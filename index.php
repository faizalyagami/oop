<?php

    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');

    $sheep = new Animal("shaun");

    echo "Name :". $sheep->name; // "shaun"
    echo "<br>";
    echo "Legs :".$sheep->legs; // 4
    echo "<br>";
    echo "Cold blooded :".$sheep->cold_blooded; // no
    echo "<br>";
    echo "<br>";
    
    
    $kodok = new Frog("buduk");
    echo "Name :".$kodok->name; // "budug"
    echo "<br>";
    echo "Legs :".$kodok->legs;
    echo "<br>";
    echo "Cold blooded :".$kodok->cold_blooded; // no
    echo "<br>";
    $kodok->jump() ; // "hop hop"
    echo "<br>";
    echo "<br>";
    
    $sungokong = new Ape("kera sakti");
    echo "Name :".$sungokong->name;
    echo "<br>";
    echo "Legs :".$sungokong->legsApe;
    echo "<br>";
    echo "Cold blooded :".$sungokong->cold_blooded; // no
    echo "<br>";
    $sungokong->yell(); // "Auooo"
    echo "<br>";
    

?>